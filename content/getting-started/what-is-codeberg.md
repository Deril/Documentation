---
eleventyNavigation:
  key: WhatIsCodeberg
  title: What is Codeberg?
  parent: GettingStarted
  order: 10
---

Codeberg is a community-driven, non-profit software development platform operated
by Codeberg e.V. and centered around Codeberg.org, a Gitea-based software forge.

On Codeberg you can develop your own [Free Software](https://simple.wikipedia.org/wiki/Free_software) projects, contribute to other
projects, [browse](https://codeberg.org/explore) through inspiring and useful
free software, share your knowledge or build your projects a home on the web
using [Codeberg Pages](/codeberg-pages), just to name a few.

Codeberg is not a corporation but a community of free software enthusiasts providing
a humane, non-commercial and privacy-friendly alternative to commercial services
such as GitHub.

Codeberg e.V. is a registered association based in Berlin, Germany. You don't have to be
a member of the association in order to join Codeberg.org or to contribute to the development
of the platform, but if you want you can [join Codeberg e.V.](https://join.codeberg.org) to
support the project financially, be informed about Codeberg and, optionally, to actively
contribute to the association.

<br>&nbsp;

To start your journey with Codeberg, let's [create an account](/getting-started/first-steps).