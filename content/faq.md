---
eleventyNavigation:
  key: FAQ
  title: Frequently Asked Questions
  icon: question
  order: 80
---

## Is Codeberg well funded?
Codeberg is primarily funded by donations. As of July 2020, with all expenses frozen, we have a runway of ~12 years.

## Where is Codeberg hosted?
We are hosted in Germany by Hetzner Online GmbH.

## What is the size limit for my repositories?
There is no fixed limit, but use cases that harm other users and projects due to excessive resource impact will get restricted. Please refer to our [terms of service](https://codeberg.org/codeberg/org/src/branch/master/TermsOfUse.md#repositories-wikis-and-issue-trackers).

## What is the size limit for my avatar?
You can upload avatar pictures of up to 1 megabyte and 1024x1024 resolution.

## Is Codeberg open-source?
Codeberg is built on [Gitea](https://github.com/go-gitea/gitea), which is open-source. We make all of our changes and other code available under the [Codeberg organization](https://codeberg.org/Codeberg).

## What version of Gitea are you running?
You can check the version of Gitea Codeberg uses through the [API here](https://codeberg.org/api/v1/version).

You will get a response like this: `{"version":"1.12.3+20-gb49f2abc5"}`. Here, 1.12.3 is the Gitea version number, and 20 is the number of patches applied on top of the release (which for example includes upstream commits and patches by Codeberg), with b49f2abc5 being the commit ID of the last patch applied on [the Codeberg branch](https://codeberg.org/Codeberg/gitea/commits/branch/codeberg) of Codeberg's Gitea repository. Note that the commit ID is without the leading "g".
