---
eleventyNavigation:
  key: ImprovingCodeberg
  title: Improving Codeberg
  icon: hands-helping
  order: 75
---

Thank you for your interest in improving Codeberg!

Every helping hand, every contribution and every donation is warmly welcome.

You can support Codeberg in various ways:

### Use Codeberg
We're happy about every free software project actively using Codeberg,
because we believe that every free project deserves a free home.

We welcome free software projects that consider moving away from one of the
commercial software forges, such as GitHub, GitLab or Bitbucket.

By joining and using Codeberg, you're already helping our mission a lot - thank you!

### Contribute to Codeberg
Do you have ideas on improving Codeberg? Have you found a bug and would like to report (or even fix) it? You're welcome to contribute to Codeberg, for example by [creating or commenting an Issue](https://codeberg.org/Codeberg/Community/issues) or by [writing a
pull request](/collaborating/pull-requests-and-git-flow) to one of Codeberg's projects.

Contributions are warmly welcome, everyone is invited!

### Donate to Codeberg
You can [donate to Codeberg e.V. using Liberapay](https://liberapay.com/codeberg/donate)
or [by SEPA wire transfer](https://codeberg.org/codeberg/org/src/branch/master/Imprint.md#user-content-sepa-iban-for-donations).

Codeberg e.V. is the non-profit organization behind Codeberg.org.

It's based in Berlin, Germany and [recognized as tax-exempt](https://codeberg.org/codeberg/org/src/branch/master/Imprint.md#user-content-gemeinn%C3%BCtzigkeit-recognition-of-status-as-non-profit-ngo-recognition-as-tax-excempt-entity) by the German authorities.

### Join Codeberg e.V.
If you're interested in committing even more to Codeberg, consider [joining Codeberg e.V.](https://join.codeberg.org), the non-profit association behind Codeberg.org.
