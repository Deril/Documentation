#!/bin/bash -ex

npm run build

rm -rf pages.git
git clone git@codeberg.org:docs/pages.git pages.git
rsync -av _site/* pages.git/
( cd pages.git && git checkout --orphan current )	## create new branch at origin
( cd pages.git && git add -A )				## add all generated files
( cd pages.git && git commit -am "deployment" )		## commit all
( cd pages.git && git branch -D main )			## delete old main branch incl. history
( cd pages.git && git branch -m main )			## rename current branch to main
( cd pages.git && git push -f origin main )		## force-push and rewrite (empty) history

